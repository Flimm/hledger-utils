[comment]: # (Keep in mind that this is a FOSS project, maintained voluntarily, so please pay attention to language.)

# New Feature

[comment]: # (Describe what the new feature could look like.)

# Benefits / Use Cases

[comment]: # (Describe what the advantages of this new feature are and some use cases.)

# Downsides / Conflicts / Backwards Compatibility

[comment]: # (If you can imagine some downsides or conflicts that this new feature could introduce, you may describe them here.)

# Technical Requirements

[comment]: # (If possible, describe what technical requirements this new feature has. Does it need an external library? Does it require a substantial structural change? ...)

# What would it look like?

[comment]: # (If it makes sense, add screenshots, sketchups, possible command-line arguments etc. that illustrate how you image the new feature to behave.)

/label ~"wish"
